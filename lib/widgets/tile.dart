import 'package:flutter/material.dart';

class AnimationTile extends StatefulWidget{
  final String title;

  AnimationTile({
    @required this.title
  });

  _AnimationTileState createState() => _AnimationTileState(title);
}

class _AnimationTileState extends State<AnimationTile> with TickerProviderStateMixin{
  final String title;

  double _height = 100.0;
  double _elevation = 2.0;

  bool _expanded = false;

  AnimationController _controller;
  Animation<Offset> _offsetFloat;


  _AnimationTileState(this.title);

  @override
  void initState() {
    super.initState();

    _controller = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 300),
    );

    _offsetFloat = Tween<Offset>(begin: Offset(-1.0, 0.0), end: Offset.zero)
        .animate(_controller);
  }

  @override
  Widget build(BuildContext context) {
    return SlideTransition(
      position: _offsetFloat,
      child: AnimatedContainer(
        duration: Duration(milliseconds: 150),
        width: MediaQuery.of(context).size.width,
        height: _height,
        child: InkWell(
          onTap: () => _handleExpansion(),
          child: Card(
            elevation: _elevation,
            child: Column(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  title,
                  style: TextStyle(
                      fontSize: 14.0
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  void _handleExpansion(){
    if(_expanded){
      setState(() {
        _height = 100;
        _elevation = 2.0;
        _expanded = false;
      });
    }
    else{
      setState(() {
        _height = 250;
        _elevation = 8.0;
        _expanded = true;
      });
    }
  }
}