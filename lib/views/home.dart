import 'package:flutter/material.dart';
import 'package:flutter_animation/constants/colors.dart';
import 'package:flutter_animation/constants/sample_data.dart';
import 'package:flutter_animation/widgets/tile.dart';

class HomePage extends StatefulWidget{

  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage>{

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Animation App',
          style: TextStyle(
            color: MyColors.white
          ),
        )
      ),
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: ListView.builder(
          padding: EdgeInsets.only(left: 8.0, right: 8.0),
          itemCount: SampleData().titles.length,
          itemBuilder: (context, index){
            return AnimationTile(
              title: SampleData().titles[index],
            );
          },
        ),
      ),
    );
  }
}