import 'package:flutter/material.dart';
import 'constants/colors.dart';
import 'views/home.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Animation',
      theme: ThemeData(
        primaryColor: MyColors.primary,
        backgroundColor: MyColors.white,
      ),
      home: HomePage(),
    );
  }
}
