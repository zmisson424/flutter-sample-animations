import 'dart:ui';

class MyColors{
  static const Color primary = Color(0xFF00BCD4);
  static const Color white = Color(0xFFFAFAFA);
  static const Color black = Color(0xFF212121);
  static const Color error = Color(0xFFd32f2f);
}